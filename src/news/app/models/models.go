package models

import (
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

var O orm.Ormer

func init() {
	orm.RegisterDataBase("default", "mysql", "root:ukf8rz5gv8@/vote?charset=utf8", 30)
	//orm.Debug = true
	orm.RegisterModel(new(People), new(RecordPeople), new(RecordNews), new(News))
	orm.RunCommand()
	O = orm.NewOrm()
}
