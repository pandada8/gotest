package models

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	"time"
)

type People struct {
	Id      int
	Title   string
	Text    string          `orm:"type(text)"`
	Records []*RecordPeople `orm:"reverse(many)"`
}

type RecordPeople struct {
	Id     int
	Time   time.Time `orm:"auto_now"`
	Ip     string
	People *People `orm:"rel(fk)"`
}

func VoteToPeople(ids []int, ip string) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = e.(error)
			O.Rollback()
			return
		} else {
			O.Commit()
			return
		}

	}()
	ni := len(ids)
	if ni != 10 {
		panic(fmt.Errorf(`必须投10票`))
	}
	if n, _ := O.QueryTable(new(RecordPeople)).Filter(`ip`, ip).Count(); n != 0 {
		panic(fmt.Errorf(`你已经投过票了`))
	}

	objs := []*People{} //People对象数组

	for i := 0; i < ni; i++ {
		objs = append(objs, new(People)) //新建n个People对象
	}

	for i := 0; i < ni; i++ {
		objs[i].Id = ids[i] //给n个People对象的ID赋值
	}

	//读取People的信息
	for i := 0; i < ni; i++ {
		err = O.Read(objs[i])
		if err != nil {
			if err == orm.ErrNoRows {
				panic(fmt.Errorf(`item ID: %d 不正确`, ids[i]))
			}
		}
	}

	//确保投票是一个事务
	err = O.Begin()
	if err != nil {
		panic(err)
	}

	rs := []*RecordPeople{}

	//新建n条投票记录
	for i := 0; i < ni; i++ {
		r := new(RecordPeople)
		r.Ip = ip
		r.People = objs[i]
		_, err = O.Insert(r)
		if err != nil {
			panic(err)
		}
		rs = append(rs, r)
	}
	return nil
}

func ShowPeopleVoteCount(id int) (count int64, err error) {
	count, err = O.QueryTable(new(RecordPeople)).Filter(`people`, id).Count()
	return
}

func GetPeopleVoteSpec(id int) (p *People, err error) {
	p = new(People)
	p.Id = id
	err = O.Read(p)
	if err != nil {
		return nil, err
	}
	_, err = O.LoadRelated(p, `records`)
	if err != nil {
		return nil, err
	}
	return
}

func GetPeopleVoteAll() (p []*People, err error) {
	_, err = O.QueryTable(new(People)).All(&p)
	if err != nil {
		return nil, err
	}
	for _, v := range p {
		_, err = O.LoadRelated(v, `records`)
		if err != nil {
			return nil, err
		}
	}

	return
}
