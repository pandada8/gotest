package controllers

import (
	"fmt"
	"github.com/robfig/revel"
	"net"
	"news/app/models"
	"strconv"
)

type item struct {
	Id       int
	Title    string
	Text     string
	Progress string
	Max      string
	Num      string
}

type News struct {
	*revel.Controller
}

func (c News) Index() revel.Result {
	narr, err := models.GetNewsVoteAll()
	if err != nil {
		c.RenderError(fmt.Errorf("Internal Error"))
	}
	max := float64(0)
	for _, v := range narr {
		if max < float64(len(v.Records)) {
			max = float64(len(v.Records))
		}
	}
	max = max / 0.8
	if max == 0 {
		max = 5.0
	}
	items := []*item{}
	for _, v := range narr {
		i := new(item)
		i.Id = v.Id
		i.Max = fmt.Sprintf("%.2f", max)
		i.Text = v.Text
		i.Title = v.Title
		i.Progress = fmt.Sprintf("%.0f", float64(len(v.Records))/max*100)
		i.Num = fmt.Sprint(len(v.Records))
		items = append(items, i)
	}
	return c.Render(items)
}

func (c News) Vote() (re revel.Result) {
	defer func() {
		if e := recover(); e != nil {
			re = c.RenderHtml(fmt.Sprintf(`<script>alert("%v");location.reload()</script>`, e.(error)))
		}
	}()
	sitems, ok := c.Params.Form[`item`]
	if !ok {
		panic(fmt.Errorf("请选择10项"))
	}

	items := []int{}
	for _, v := range sitems {
		i, err := strconv.ParseInt(v, 10, 0)
		if err != nil {
			panic(err)
		}
		items = append(items, int(i))
	}

	var ip string

	if ip = c.Request.Header.Get(`x-forward-for`); ip == `` {
		ip = c.Request.RemoteAddr
	}
	ip, _, err := net.SplitHostPort(ip)
	if err != nil {
		panic(ip)
	}
	err = models.VoteToNews(items, ip)
	if err != nil {
		panic(err)
	}
	panic(fmt.Errorf(`投票成功`))
}

type People struct {
	*revel.Controller
}

func (c People) Index() revel.Result {
	narr, err := models.GetPeopleVoteAll()
	if err != nil {
		c.RenderError(fmt.Errorf("Internal Error"))
	}
	max := float64(0)
	for _, v := range narr {
		if max < float64(len(v.Records)) {
			max = float64(len(v.Records))
		}
	}
	max = max / 0.8
	if max == 0 {
		max = 5.0
	}
	items := []*item{}
	for _, v := range narr {
		i := new(item)
		i.Id = v.Id
		i.Max = fmt.Sprintf("%.2f", max)
		i.Text = v.Text
		i.Title = v.Title
		i.Progress = fmt.Sprintf("%.0f", float64(len(v.Records))/max*100)
		i.Num = fmt.Sprint(len(v.Records))
		items = append(items, i)
	}

	return c.Render(items)
}

func (c People) Vote() (re revel.Result) {
	defer func() {
		if e := recover(); e != nil {
			re = c.RenderHtml(fmt.Sprintf(`<script>alert("%v");location.reload()</script>`, e.(error)))
		}
	}()
	sitems, ok := c.Params.Form[`item`]
	if !ok {
		panic(fmt.Errorf("请选择10项"))
	}

	items := []int{}
	for _, v := range sitems {
		i, err := strconv.ParseInt(v, 10, 0)
		if err != nil {
			panic(err)
		}
		items = append(items, int(i))
	}

	var ip string

	if ip = c.Request.Header.Get(`x-forward-for`); ip == `` {
		ip = c.Request.RemoteAddr
	}
	ip, _, err := net.SplitHostPort(ip)
	if err != nil {
		panic(fmt.Errorf(`你的ip地址有问题啊`))
	}
	err = models.VoteToPeople(items, ip)
	if err != nil {
		panic(err)
	}
	panic(fmt.Errorf(`投票成功`))
}
